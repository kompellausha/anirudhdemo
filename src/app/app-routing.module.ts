import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApisComponentComponent } from './apis-component/apis-component.component';
import { SoefilesComponentComponent } from './soefiles-component/soefiles-component.component';
import { TeamsComponentComponent } from './teams-component/teams-component.component';

const routes: Routes = [
  {path:'',pathMatch:"full",redirectTo:"files"},
  {path:"files",component:SoefilesComponentComponent},
  {path:'teams',component:TeamsComponentComponent},
  {path:"apis",component:ApisComponentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
