import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoefilesComponentComponent } from './soefiles-component.component';

describe('SoefilesComponentComponent', () => {
  let component: SoefilesComponentComponent;
  let fixture: ComponentFixture<SoefilesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoefilesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoefilesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
