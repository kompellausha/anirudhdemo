import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApisComponentComponent } from './apis-component.component';

describe('ApisComponentComponent', () => {
  let component: ApisComponentComponent;
  let fixture: ComponentFixture<ApisComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApisComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApisComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
